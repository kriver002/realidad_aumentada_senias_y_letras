﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// HUD manager. Es un patrón singletón (solo puede existir uno),
/// que le hereda a Monobehavior
/// </summary>
public class HUDManager : MonoBehaviour
{
	private static HUDManager 	instance = null;
	public Animator animatorMenu;
	public Animator animatorScanner;
    public Text txtReceta;


    public static HUDManager Instance
	{
		get { return instance; }
		set { instance = value; }
	}

	void Awake()
	{
		// Si ya se reservó memoria a la instancia
		// y no es esta instancia
		if (Instance != null && Instance != this)
		{
			Destroy(this);
			return;
		}
		Instance = this;
	}
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

	public void MostarMenu(bool mostrar)
	{
       
		animatorMenu.SetBool ("mostrar", mostrar);
	}

	public void MostarScanner(bool mostrar)
	{
        animatorMenu.SetTrigger("marcadorPerdido");
		animatorScanner.SetBool ("mostrarScanner", mostrar);
        animatorMenu.SetBool("marcadorEncontado", false);

	}
    public void MostarTxtReceta(bool mostrar)
    {
        if(mostrar == false )
        {
            txtReceta.text = "";
            txtReceta.gameObject.SetActive(false);
        }
        else { txtReceta.gameObject.SetActive(true); }
    }

    public void MarcadorEncontrado(bool marcador)
    {
        animatorMenu.SetBool("marcadorEncontado", marcador);
    }

    public void CerrarMenu()
    {
        animatorMenu.SetBool("marcadorEncontado", true);
        animatorMenu.SetBool("mostrar", false);
    }

}
 