﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class ARTrackerEventHandler : MonoBehaviour,
									 ITrackableEventHandler
{
	private TrackableBehaviour  mTrackableBehaviour;
    private Animator            copia;

    public Animator             animatorPersonaje;
   

	void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
        copia = animatorPersonaje;
	}

	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
		    newStatus == TrackableBehaviour.Status.TRACKED ||
		    newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			OnTrackingFound();
		}
		else
		{
			OnTrackingLost();
		}
	}

	private void OnTrackingFound()
	{
		//HUDManager.Instance.MostarMenu (true);
		HUDManager.Instance.MostarScanner (false);
        HUDManager.Instance.MostarTxtReceta(true);
        HUDManager.Instance.MarcadorEncontrado(true);
	}

	private void OnTrackingLost()
	{
        HUDManager.Instance.MostarMenu (false);
		HUDManager.Instance.MostarScanner (true);
        HUDManager.Instance.MostarTxtReceta(false);
        PersonajeManager.Instance.PararCorrutina();
        HUDManager.Instance.MarcadorEncontrado(false);
    }
}
