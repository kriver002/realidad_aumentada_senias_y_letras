﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PersonajeManager : MonoBehaviour
{

    public Animator animatorPersonaje;
    public Text txtNombreAnimacion;
    public Text txtRecetaActual;

    private static PersonajeManager instance = null;
    private bool idle = true;
    private bool marcadorPerdido = false;
    private bool botonActivo = true;

    public static PersonajeManager Instance
    {
        get { return instance; }
        set { instance = value; }
    }

    void Awake()
    {
        // Si ya se reservó memoria a la instancia
        // y no es esta instancia
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this;
    }

    void Start()
    {
        GameInformation.LoadFile();
    }


    IEnumerator EsperarAnimacion(int idReceta)
    {
        Debug.LogError(GameInformation.recetas[idReceta].Nombre);
        txtRecetaActual.text = GameInformation.recetas[idReceta].Nombre;
        int numTotalAnimaciones = GameInformation.recetas[idReceta].Animaciones.Count;
        int animActual = 0;

        foreach (Animacion animacion in GameInformation.recetas[idReceta].Animaciones)
        {
            animActual++;
            animatorPersonaje.SetFloat("idAnimacion", animacion.Id);
            float tiempoAnimacion = ObtenerTiempoAnimacion(animacion.Nombre);
            Debug.LogError("ESA ANIMANDO " + animacion.Nombre);
            Debug.LogError("TEXYYO DE ANIMACION " + txtNombreAnimacion.text);
            animatorPersonaje.CrossFade("Blend Tree", 0.1f, -1, 0); // Interpola la animación
            if (animActual == numTotalAnimaciones)
            {
                idle = true;
                animatorPersonaje.SetBool("idle", idle);
                //botonActivo = true;
                //DebugVisual.Log("Titulo", "lhhas", MensajeColor.Red)
            }

            if (marcadorPerdido)
            {
                animatorPersonaje.Rebind();
                animatorPersonaje.SetTrigger("reinicializarAnimacion");
                yield return new WaitForSeconds(0);
            }
            //Tiempo de espera para hacer la otra animación
            txtNombreAnimacion.text = animacion.NombreVisual;
            yield return new WaitForSeconds(tiempoAnimacion);
        }
        txtRecetaActual.text = "";
        txtNombreAnimacion.text = "";
        yield return null;
    }

    public void PararCorrutina()
    {
        txtRecetaActual.text = "";
        animatorPersonaje.Rebind();
        animatorPersonaje.SetTrigger("reinicializarAnimacion");
        //botonActivo = true;

        StopAllCoroutines();
        marcadorPerdido = true;
    }

    public float ObtenerTiempoAnimacion(string nombreAnimacion)
    {
        RuntimeAnimatorController ac = animatorPersonaje.runtimeAnimatorController;

        for (int i = 0; i < ac.animationClips.Length; i++)
        {
            if (ac.animationClips[i].name == nombreAnimacion)
            {
                return ac.animationClips[i].length;
            }
        }
        return 0.0f;
    }

    void IniciarReceta(int idR)
    {
        marcadorPerdido = false;

        StopAllCoroutines();
        StartCoroutine(EsperarAnimacion(idR));

    }

    public void AnimarPersonje(int id)
    {
        IniciarReceta(id);
        idle = false;
    }

    public void ObtnerYasignarAnimator()
    {
        if (!this.gameObject.GetComponent<Animator>())
        {
            Animator animator = this.gameObject.AddComponent<Animator>();
            animator.runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("3D/Luigi/Animator Luigi", typeof(RuntimeAnimatorController));
        }
    }

    public void DestruirAnimator()
    {
        if (this.gameObject.GetComponent<Animator>())
            Destroy(this.gameObject.GetComponent<Animator>());
    }

}