﻿using UnityEngine;
using System.Collections;

public class CambiarAnimacionPersonaje : MonoBehaviour
{
	public Animator animatorPersonaje;

	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.Mouse0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast(ray, out hit, 100))
			{
				if (hit.collider.tag == "Player")
				{
					animatorPersonaje.SetFloat("idAnimacion", Random.Range(0, 7) );
				}
			}
		}
	}
}
