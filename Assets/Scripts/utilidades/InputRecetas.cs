﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class InputRecetas : MonoBehaviour {

	public GameObject btnEjecutarAnimacion;
	public Transform pnlRecetas;
	public PersonajeManager pm;

	void Start ()
	{
		GameInformation.LoadFile();
		ObtenerTodasRecetas();

        //CrearMensajes();
	}

	void CrearMensajes ()
	{
        for (int i = 0; i < 100; i++)
        {
            DebugVisual.Log("Prueba Recetas", "mostar receta", MensajeColor.Blanco);
        }
        
	}

	private void ObtenerTodasRecetas()
	{
		// Se crean los botones en tiempo real del autocompletar
		CrearBotonesAutocompletar(GameInformation.recetas);
	}

	public void CrearBotonesAutocompletar(List<Receta> TodasRecetas)
	{
		// Se crean los botones en tiempo real del autocompletar
		foreach (Receta recetasAnimaciones in TodasRecetas)
		{
			// Se crea el botón en el panel contendor
			GameObject btnCreado = (GameObject)Instantiate(btnEjecutarAnimacion, pnlRecetas.position, Quaternion.identity);
			btnCreado.transform.SetParent(pnlRecetas);
			btnCreado.transform.localScale = Vector3.one;

			// Se cambia el texto del boton y se enriquece con etiquetas html
			Text textoBoton = btnCreado.GetComponentInChildren<Text>();
			textoBoton.text = recetasAnimaciones.Nombre;

			// Se obtiene el componente Boton
			Button refBotonCreado = btnCreado.GetComponent<Button>();
			// Se crean los objetos para que apunten a una nueva direccion de memoria

			string nombreReceta = new string(recetasAnimaciones.Nombre.ToCharArray());
			int idReceta =  recetasAnimaciones.Id;
			// Se crea el delegado cuando se presiona el boton
			refBotonCreado.onClick.AddListener(delegate { pm.AnimarPersonje(idReceta); });
		}
	}
}
