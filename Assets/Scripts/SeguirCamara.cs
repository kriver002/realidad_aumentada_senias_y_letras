﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SeguirCamara : MonoBehaviour {

	public Camera camara ;
	public Text txt_animacionActual;

	void Update ()
	{
		if(camara != null)
			this.transform.rotation = camara.transform.rotation;
	}
}
