﻿using System.Collections.Generic;


public class Juego
{
	private List<Receta> recetas = new List<Receta> ();
	private bool		sonido = true;
	private bool		musica = true;

	public List<Receta> Recetas 
	{
		get {return recetas;}
		set {recetas = value;}
	}

	public bool Sonido
	{
		get { return sonido; }
		set { sonido = value; }
	}

	public bool Musica {
		get { return musica; }
		set { musica = value; }
	}
}
