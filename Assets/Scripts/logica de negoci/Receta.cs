﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Receta 
{
	private string nombre;
	private int    id;

	private List<Animacion> animaciones = new List<Animacion>();

	public string Nombre {
		get {return nombre;}
		set {nombre = value;}
	}

	public int Id {
		get {return id;}
		set {id = value;}
	}

	public List<Animacion> Animaciones
	{
		get {return animaciones;}
		set {animaciones = value;}
	}

	public Receta(int newId, string newNombre)
	{
		this.id = newId;
		this.nombre = newNombre;
	}

	public void AgregarAnimaciones(string palabrasReceta, List<Animacion> todasAnimaciones)
	{
		string[] palabra = palabrasReceta.Split (',');

		foreach (string item in palabra)
		{
			foreach (Animacion anim in todasAnimaciones)
			{
				if (anim.Nombre == item) 
				{
					animaciones.Add (anim);
				}
			}
		}
	}
}
