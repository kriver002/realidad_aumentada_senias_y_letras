﻿using UnityEngine;
using System.Collections;

public class Animacion
{
	private int		id = -1;
	private string	nombre = "";
	private string	nombreVisual ="";

	public int Id {
		get { return id; }
		set { id = value; }
	}

	public string Nombre {
		get { return nombre; }
		set { nombre = value; }
	}

	public string NombreVisual {
		get {return nombreVisual;}
		set {nombreVisual = value;}
	}

	/// <summary>
	/// Constructor por defecto de la clase
	/// </summary>
	public Animacion(){}

	/// <summary>
	/// Constructor que recibe los parametros por defecto del objeto
	/// </summary>
	/// <param name="newId">Identificador de la animacion</param>
	/// <param name="newNombre">Nombre de la animacion</param>
	public Animacion(int newId, string newNombre,string newNV)
	{
		this.id = newId;
		this.nombre = newNombre;
		this.nombreVisual = newNV;
	}
}
