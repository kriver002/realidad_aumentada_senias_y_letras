﻿using UnityEngine;
using System.Collections.Generic;
public static class GameInformation
{
	private static Juego seniasYLetras                  = new Juego();
	public static List<Receta> recetas                  = new List<Receta>(); 
	public static List<Animacion> animacionesPalabras   = new List<Animacion>(); 

	public static Juego SeniasYLetras
	{
		get { return seniasYLetras; }
		set { seniasYLetras = value; }
	}

	/// <summary>
	/// Se limpia restablecen los valores por defecto del GameInformacion y el UnityUtility
	/// </summary>
	public static void Reset()
	{
		seniasYLetras = new Juego();
		UnityUtility.Reset();
	}

	public static void LoadFile()
	{		
		CrearInformacionAnimaciones ();	
    }

	static void CrearInformacionAnimaciones()
	{
		animacionesPalabras.Add (new Animacion (0, "aceite","Aceite"));
		animacionesPalabras.Add (new Animacion (1, "arroz","Arroz"));
		animacionesPalabras.Add (new Animacion (2, "freir","Freir"));
		animacionesPalabras.Add (new Animacion (3, "mezclar","Mezclar"));
		animacionesPalabras.Add (new Animacion (4, "hoy","Hoy"));
		animacionesPalabras.Add (new Animacion (5, "pollo","Pollo"));
		animacionesPalabras.Add (new Animacion (6, "paila","Paila"));
		animacionesPalabras.Add (new Animacion (7, "quevamoshacerhoy","Que vamos hacer"));

		Receta objReceta = new Receta (0, "Arroz y pollo frito");
        objReceta.AgregarAnimaciones("hoy,quevamoshacerhoy,arroz,pollo,paila,freir", animacionesPalabras);
		Receta objReceta1 = new Receta (1, "Arroz con pollo");
        objReceta1.AgregarAnimaciones("hoy,quevamoshacerhoy,arroz,pollo,mezclar", animacionesPalabras);
		recetas.Add (objReceta);
		recetas.Add (objReceta1);
	}
}
