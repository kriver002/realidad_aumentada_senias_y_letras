﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DebugVisual 
{
    //public static Queue<Log> mensajes = new Queue<Log>();

    public static List<Log> mensajes = new List<Log>();

    public static void Log(string titulo, object mensaje, MensajeColor color)
    {
        Log m = new Log(titulo, mensaje, color);
        mensajes.Add(m);
        //Llamar evento
        EventManager.Evento_CargarMensaje();
    }

    public static void Log(string titulo, object mensaje)
    {
        Log m = new Log(titulo, mensaje, MensajeColor.Rojo);
        mensajes.Add(m);
        //Llamar evento
        EventManager.Evento_CargarMensaje();
    }

    public static string ObtenerMensajes()
    {
        string mensajesLog = "";

        foreach (Log msn in DebugVisual.mensajes)
        {
            mensajesLog += msn.ObtenerMensaje();
        }

        return mensajesLog;
        //Debug.LogError(mensajes[DebugVisual.mensajes.Count - 1].ObtenerMensaje());
        //return mensajes[DebugVisual.mensajes.Count - 1].ObtenerMensaje();

    }
}

