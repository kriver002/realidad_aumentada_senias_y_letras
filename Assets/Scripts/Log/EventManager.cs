﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void CargarMensaje();

public class EventManager
{
    private static CargarMensaje evento_CargarMensaje;

    public static  CargarMensaje Evento_CargarMensaje
    { 
        get {return evento_CargarMensaje;}
        set {evento_CargarMensaje = value;}
    }
}
