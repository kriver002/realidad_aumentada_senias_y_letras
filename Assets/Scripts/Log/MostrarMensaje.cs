﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MostrarMensaje : MonoBehaviour
{
    public Text txtMostrarMensaje;
    public Text txtFPS;
    private bool ocultar = false;
    public GameObject panelLog;
    public ScrollRect scroll;

    private float a = 0.0f;
    private float frames = 0.0f;
    private float timeleft = 0.5f;

    void Awake()
    {
        //InvokeRepeating("CrearMensajes", 1.0f, 0.5f);
        EventManager.Evento_CargarMensaje += this.CargarMensaje;
    }

    void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            txtMostrarMensaje.resizeTextForBestFit = true;
            txtMostrarMensaje.resizeTextMinSize = 10;
            txtMostrarMensaje.resizeTextMaxSize = 35;
        }
        else
            txtMostrarMensaje.resizeTextForBestFit = false;

        //DebugVisual.Log("pruebacolor", "sin mandar color");
        DebugVisual.Log("Prueba int", 98, MensajeColor.Amarillo);
        DebugVisual.Log("Prueba string", "La vida es bella", MensajeColor.Rojo);
        DebugVisual.Log("Prueba bool", ocultar, MensajeColor.Blanco);
        DebugVisual.Log("Prueba int", 98, MensajeColor.Amarillo);
        DebugVisual.Log("Prueba string", "La vida es bella", MensajeColor.Rojo);
        DebugVisual.Log("Prueba bool", ocultar, MensajeColor.Blanco);
        DebugVisual.Log("Prueba vector3", "hola prueba de color rojo");
        DebugVisual.Log("Prueba int", 98, MensajeColor.Amarillo);
        DebugVisual.Log("Prueba string", "La vida es bella", MensajeColor.Rojo);
        DebugVisual.Log("Prueba bool", ocultar, MensajeColor.Blanco);
        DebugVisual.Log("Prueba int", 98, MensajeColor.Amarillo);
        DebugVisual.Log("Prueba string", "La vida es bella", MensajeColor.Rojo);
        DebugVisual.Log("Prueba bool", ocultar, MensajeColor.Blanco);
        DebugVisual.Log("Prueba vector3", "hola prueba de color rojo");
        DebugVisual.Log("Prueba int", 98, MensajeColor.Amarillo);
        DebugVisual.Log("Prueba string", "La vida es bella", MensajeColor.Rojo);
        DebugVisual.Log("Prueba bool", ocultar, MensajeColor.Blanco);
        DebugVisual.Log("Prueba int", 98, MensajeColor.Amarillo);
        DebugVisual.Log("Prueba string", "La vida es bella", MensajeColor.Rojo);
        DebugVisual.Log("Prueba bool", ocultar, MensajeColor.Blanco);
        DebugVisual.Log("Prueba vector3", "hola prueba de color rojo");


    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) || (Input.touches.Length >= 3 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
            OcultarLog(ocultar);
        }
    }

    private void CargarMensaje()
    {
        txtMostrarMensaje.text = DebugVisual.ObtenerMensajes();
        Canvas.ForceUpdateCanvases();
        scroll.verticalNormalizedPosition = 0;
        Canvas.ForceUpdateCanvases();
    }

    public void OcultarLog(bool pOcultar)
    {
        panelLog.SetActive(pOcultar);
        ocultar = !pOcultar;
    }

    void CrearMensajes()
    {
        DebugVisual.Log("Prueba Recetas", "mostar receta "+DebugVisual.mensajes.Count, MensajeColor.Amarillo);
    }
}
