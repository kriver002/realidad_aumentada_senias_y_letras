﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Log
{
    //private Queue<> DebugLog = new Queue<?>();

    private string titulo;
    private MensajeColor color;
    private object mensaje;

    public Log(string ptitulo, object pmensaje, MensajeColor pcolor)
    {
        this.titulo = ptitulo;
        this.mensaje = pmensaje;
        this.color = pcolor;
    }
  
    public string Titulo
    {
        get { return titulo; }
        set { titulo = value; }
    }

    public MensajeColor Color
    {
        get { return color; }
        set { color = value; }
    }

    public object Mensje
    {
        get { return mensaje; }
        set { mensaje = value; }
    }

    public string ObtenerMensaje()
    {
        return "<b><color=" + ObtenerHexColor() + ">" + titulo + "</color></b>\n" + mensaje.ToString() + "\n";
    }

    private string ObtenerHexColor()
    {
        if (color == MensajeColor.Amarillo)
            return "#ffff00";
        else if (color == MensajeColor.Blanco)
            return "#FFFFFF";
        return "#FF0000";
    }
}
